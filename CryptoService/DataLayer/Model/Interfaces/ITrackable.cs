﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoService.DataLayer.Model.Interfaces
{
    //keeps track of changes in db fields
    public interface ITrackable
    {
        DateTimeOffset CreatedAt { get; set; }
        DateTimeOffset LastUpdatedAt { get; set; }
        string CreatedBy { get; set; }
        string LastUpdatedBy { get; set; }
    }
}
