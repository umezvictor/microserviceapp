﻿using CryptoService.DataLayer.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoService.DataLayer.Model
{
    public class Transaction : ISoftDeletable, ITrackable
    {
        public Guid TransactionId { get; set; }
        public string WalletAddress { get; set; }
        public string CurrencyType { get; set; }       
        public decimal Amount { get; set; } 
        public decimal Balance { get; set; }
        
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset LastUpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }
     
        //foreign key
        public Guid ClientId { get; set; }
        public AppUser Client { get; set; }

    }


}
