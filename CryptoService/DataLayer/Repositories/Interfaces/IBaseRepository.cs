﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService.DataLayer.Repositories.Interfaces
{
    //to avoid repeating ourself
    //this interface will be used to have a generic repository
    public interface IBaseRepository<T> where T : class
    {
       
        IQueryable<T> QueryAll(Expression<Func<T, bool>> expression = null); //for any kind of query for multiple data
        Task<List<T>> GetList(Expression<Func<T, bool>> expression = null);
        Task CreateAsync(T entry);
        Task CreateRangeAsync(List<T> entryList);
        void Update(T entry);
        void UpdateRange(List<T> entryList);//for updating multiple records
        void Delete(T entry);
        void DeleteRange(List<T> entryList);
        Task<T> FindSingleAsync(Expression<Func<T, bool>> expression);
        //Task<bool> SaveCompletedAsync();

    }
}
