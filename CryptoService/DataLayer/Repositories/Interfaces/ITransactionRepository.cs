﻿using CryptoService.DataLayer.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoService.DataLayer.Repositories.Interfaces
{
    public interface ITransactionRepository : IBaseRepository<Transaction>
    {
        
    }
}