﻿using CryptoService.DataLayer;
using CryptoService.DataLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService.DataLayer.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppDbContext _context;
        private bool disposed = false;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }
        private ITransactionRepository _transactionRepository;
       

        public ITransactionRepository TransactionRepository =>
            _transactionRepository ??= new TransactionRepository(_context);

       

       
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
           
        }
        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
