﻿using CryptoService.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoService.Services
{
    public interface ITransactionService
    {
        Task<List<Transaction>> GetAllAsync(string clientId, string walletAddress);
       
        Task InsertDummyUsers();
    }
}
