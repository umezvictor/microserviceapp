﻿using Bogus;
using CryptoService.DataLayer.Model;
using CryptoService.DataLayer.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoService.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AppUser> _userManager;

        public TransactionService(IUnitOfWork unitOfWork, UserManager<AppUser> userManager)
        {

            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public async Task<List<Transaction>> GetAllAsync(string clientId, string walletAddress)
        {
            var response = await _unitOfWork.TransactionRepository.GetList();
            var transactions = response.Where(x => x.ClientId == Guid.Parse(clientId.Trim()) && x.WalletAddress == walletAddress.Trim());
            return transactions.ToList();
        }

       

        public async Task InsertDummyUsers()
        {
            var userList = new List<AppUser>()
           {
               new AppUser()
               {
                   UserName = "victorblaze2010@gmail.com",
                   Email = "victorblaze2010@gmail.com",

                   FullName = "chibuzor Victor"
               },

                new AppUser()
                {
                    UserName = "info.vteck@gmail.com",
                    Email = "info.vteck@gmail.com",
                    FullName = "Victor Umezuruike"
                }
            };

            foreach(var user in userList)
            {
                 await _userManager.CreateAsync(user, "#Vi123456");
            }
           
        }
    }
}
