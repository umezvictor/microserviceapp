﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class TransactionResponse
    {
        public List<ClientTransaction> Transactions { get; set; }
    }


   
    public class ClientTransaction
    {
        public string TransactionId { get; set; }
        public string WalletAddress { get; set; }
        public string CurrencyType { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset LastUpdatedAt { get; set; }
        public object CreatedBy { get; set; }
        public object LastUpdatedBy { get; set; }
        public string ClientId { get; set; }
        public object Client { get; set; }
    }



}
