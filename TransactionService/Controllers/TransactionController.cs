﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionService.DataLayer.Dto;
using TransactionService.DataLayer.Model;
using TransactionService.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TransactionService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {

        private readonly IBus bus;
        private readonly IRequestClient<UpdateTransactions> client;
        private readonly ITransactionsService transactionsService;
        private readonly IRequestService requestService;

        public TransactionController(IBus bus, IRequestClient<UpdateTransactions> client,
            ITransactionsService transactionsService, IRequestService requestService)
        {
            this.bus = bus;
            this.client = client;
            this.transactionsService = transactionsService;
            this.requestService = requestService;
        }

        // GET: api/<TransactionController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {

            var transactions = await transactionsService.GetAllAsync();
            if(transactions != null)
            {
                return Ok(transactions);
            }
            return NotFound();
        }

        // GET api/<TransactionController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var transactions = await transactionsService.GetAllAsync(id);
            if (transactions != null)
            {
                return Ok(transactions);
            }
            return NotFound();
        }

       

        // POST api/<TransactionController>
        [HttpPost("all")]
        public async Task<IActionResult> Post([FromBody] UpdateTransactions data)
        {
            var request = client.Create(data);
            var response = await request.GetResponse<TransactionResponse>();

            //if success - publish TransactionReceived event to the event bus
            if(response.Message.Transactions.Count() > 0)
            {
                await bus.Publish(new TransactionReceived()
                {
                    message = "Transaction Received"
                   
                });

                RequestDto requestDto = new RequestDto()
                {
                    RequestId = Guid.NewGuid(),
                    CreatedAt = DateTimeOffset.Now
                };
                await requestService.InsertAsync(requestDto);


                //save transactions
                foreach (var item in response.Message.Transactions)
                {
                    Transaction transaction = new Transaction();
                    transaction.Amount = item.Amount;
                    transaction.Balance = item.Balance;
                    transaction.ClientId = item.ClientId;
                    transaction.CreatedAt = item.CreatedAt;
                    transaction.Id = Guid.NewGuid();
                    transaction.WalletAddress = item.WalletAddress;
                    transaction.RequestId = requestDto.RequestId;
                    await transactionsService.InsertAsync(transaction);
                }
                return Ok(new { message = "all ok"});
            }
            //save unique request
            return Ok(new { message = "no record retrieved from crypto api" });


        }

        
    }
}
