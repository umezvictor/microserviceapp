﻿using TransactionService.DataLayer.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TransactionService.DataLayer.Repositories.Interfaces
{
    public interface IRequestRepository : IBaseRepository<Request>
    {
        
    }
}