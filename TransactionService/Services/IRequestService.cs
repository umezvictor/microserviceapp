﻿using TransactionService.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionService.DataLayer.Dto;

namespace TransactionService.Services
{
    public interface IRequestService
    {
       
        Task InsertAsync(RequestDto request);
    }
}
