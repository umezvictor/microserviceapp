﻿using TransactionService.DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionService.DataLayer.Dto;

namespace TransactionService.Services
{
    public interface ITransactionsService
    {
        Task<List<Transaction>> GetAllAsync();
        Task<List<Transaction>> GetAllAsync(string clientId);
        Task InsertAsync(Transaction request);
        Task<Transaction> GetSingleAsync(string Id);
    }
}
