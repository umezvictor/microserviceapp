﻿using Bogus;
using TransactionService.DataLayer.Model;
using TransactionService.DataLayer.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransactionService.DataLayer.Dto;

namespace TransactionService.Services
{
    public class RequestService : IRequestService
    {
        private readonly IUnitOfWork _unitOfWork;


        public RequestService(IUnitOfWork unitOfWork)
        {

            _unitOfWork = unitOfWork;
            
        }

      

        public async Task InsertAsync(RequestDto request)
        {

            Request requestObject = new Request();
            requestObject.RequestId = request.RequestId;
            requestObject.CreatedAt = request.CreatedAt;

            await _unitOfWork.RequestRepository.CreateAsync(requestObject);
            await _unitOfWork.SaveChangesAsync();
           
   
        }


    }
}
